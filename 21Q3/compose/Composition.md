## Composition
### 设计细节

#### ref

get track, set trigger <- customRef
triggerRef

*** 实现 ***

#### reactive

track() 与 trigger()

toRef
```js
const obj = reactive({ foo: 1 })
const obj2 = { foo: toRef(obj, 'foo') } // 修改了这里

effect(() => {
  console.log(obj2.foo.value)  // 由于 obj2.foo 现在是一个 ref，因此要访问 .value
})

obj.foo = 2 // 有效
```

*** 实现 ***

**自动脱 ref**

#### toRef
```js
function toRef(target, key) {
    return {
        isRef: true,
        get value() {
            return target[key]
        },
        set value(newVal){
            target[key] = newVal
        }
    }
}
```


#### toRefs
解构响应式对象的 property 会导致响应式丢失（如果 property 还是对象呢？），但是我们可以使用 toRefs 将 property 转换成 ref。

```js
function toRefs(target){
    const ret: any = {}
    for (const key in target) {
        ret[key] = toRef(target, key)
    }
    return ret
} 
```

#### readonly
https://v3.cn.vuejs.org/guide/reactivity-fundamentals.html#%E4%BD%BF%E7%94%A8-readonly-%E9%98%B2%E6%AD%A2%E6%9B%B4%E6%94%B9%E5%93%8D%E5%BA%94%E5%BC%8F%E5%AF%B9%E8%B1%A1


```js
import { reactive, toRefs } from 'vue'

const book = reactive({
  author: 'Vue Team',
  year: '2020',
  title: 'Vue 3 Guide',
  description: 'You are reading this book right now ;)',
  price: 'free'
})

let { author, title } = toRefs(book)

title.value = 'Vue 3 Detailed Guide' // 我们需要使用 .value 取值，因为它现在是 ref
console.log(book.title) // 'Vue 3 Detailed Guide'
```

#### markRaw
实际上 markRaw 函数所做的事情，就是在数据对象上定义 __v_skip 属性，从而跳过代理。

#### 组件状态
类似 data，setup 可以返回一个对象，这个对象上的属性将会被暴露给模版的渲染上下文。

```js
const MyComponent = {
  props: {
    name: String
  },
  setup(props) {
    return {
      msg: `hello ${props.name}!`
    }
  },
  template: `<div>{{ msg }}</div>`
}
```

这里的 setup 和 data 的作用一致。模板中也能通过 this.msg 修改 msg 的值。

如果我们想要创建一个可以在 setup() 内部被管理的值，可以使用 ref 函数。

```js
setup(props) {
  const msg = ref('hello')
  const appendName = () => {
    msg.value = `hello ${props.name}`
  }
  return {
    msg,
    appendName
  }
},
```

最终，我们就可以在封装了逻辑的组合函数中将状态以引用的方式传回给组件。组件负责展示（追踪依赖），组合函数负责管理状态（触发更新）：

```js
setup() {
  const valueA = useLogicA() // valueA 可能被 useLogicA() 内部的代码修改从而触发更新
  const valueB = useLogicB()
  return {
    valueA,
    valueB
  }
}
```

#### 直接返回渲染函数（jsx？）
```js
import { ref, createElement as h } from 'vue'

const MyComponent = {
  setup(initialProps) {
    const count = ref(0)
    const increment = () => { count.value++ }

    return (props, slots, attrs, vnode) => (
      h('button', {
        onClick: increment
      }, count.value)
    )
  }
}
```

https://link.zhihu.com/?target=https%3A//github.com/vuejs/rfcs/pull/28

### 部分原理
#### 返回的 state 怎么和模板建立联系？
Vue.js 2.x 编写组件的时候，会在 props、data、methods、computed 等 options 中定义一些变量。在组件初始化阶段，Vue.js 内部会处理这些 options，即把定义的变量添加到了组件实例上。等模板编译成 render 函数的时候，内部通过 with(this){} 的语法去访问在组件实例中的变量。

那么到了 vue3.0 的时候是怎么访问到 setup 函数中返回的值的呢？

### 在业务中使用
#### router
https://github.com/vuejs/composition-api/issues/320

```js
beforeRouteEnter(to, from, next) {
  next((vm) => {
    vm.onRouteUpdate();
  });
}
```

https://github.com/vuejs/composition-api/issues/49#issuecomment-701015268
```js
import Vue from 'vue'
import { getCurrentInstance } from '@vue/composition-api'
import { NavigationGuard } from 'vue-router'
import { ComponentOptions } from 'vue/types/umd'

export function onHook(
  name: keyof ComponentOptions<Vue>,
  callback: (...args: any) => void
) {
  const vm = getCurrentInstance()
  const merge = Vue.config.optionMergeStrategies[name]
  if (vm && merge) {
    const prototype = Object.getPrototypeOf(vm.proxy.$options)
    prototype[name] = merge(vm.proxy.$options[name], callback)
  }
}

export function onBeforeRouteUpdate(callback: NavigationGuard<Vue>) {
  return onHook('beforeRouteUpdate', callback)
}

export function onBeforeRouteLeave(callback: NavigationGuard<Vue>) {
  return onHook('beforeRouteLeave', callback)
}
```

### 可能存在的问题
与 store 一起使用：useStore？（https://next.vuex.vuejs.org/guide/composition-api.html#accessing-state-and-getters）

ref 是深层次的嘛？能对 push 响应嘛？

如果将新的 ref 赋值给现有 ref 的 property，将会替换旧的 ref：

reactive(ref(1))?

readonly 还算响应式吗？
> 不算 isReactive，但算 isProxy，是代理对象

### 缺点/潜在问题
很容易写乱。
https://www.zhihu.com/question/416652570/answer/1427211687

丢失响应式：
传递参数不加 toRef
```
export default {
  setup(props) {
    useSomeFeature(toRef(props, 'foo'))
  }
}
```

## more

https://zhuanlan.zhihu.com/p/81012971
https://www.zhihu.com/question/429036806

https://hkc452.github.io/slamdunk-the-vue3/main/

```js
setup() {
  const obj = reactive({ foo: 1 })
  return { ...obj } // 响应式会丢失
}

setup() {
  const obj = reactive({ foo: 1 })
  return { ...toRefs(obj) } // 响应式不会丢失，但是需要加 .value
}

setup() {
  const obj = reactive({ foo: 1 })
  return reactive({ ...toRefs(obj) })
}
```