## keep-alive
`<keep-alive>` 是 vue 内置的一个抽象组件，与 `<transition>` 类似，自身不会渲染实际的 DOM 元素。它的作用是当用 `<keep-alive>` 包裹动态组件时，会缓存不活动的组件实例，而不是销毁它们。下面我将介绍 `<keep-alive>` 具体是如何实现的，我会尽量不涉及大量源码，并在后面附上相关链接让有兴趣的同学自己下面去看。当然本质上，它的实现原理用一句话就可以描述，那就是缓存已经创建过的 vnode。

我们知道通常的组件都有一个挂载和销毁的生命周期，就是 `mounted` 和 `destroyed`，但是 `<keep-alive>` 组件通过 `options.abstract` 来跳过 vue 对该组件包裹的 children 的渲染，然后自己实现了一个 render 函数，而不是我们常规模板的方式，这样就可以对所 render 的内容进行缓存。

> 需要注意的是 `<keep-alive>` 只处理第一个子元素，所以一般搭配 component 动态组件或者 `<router-view>` 使用。
> 当组件在 `<keep-alive>` 内被切换，它的 activated 和 deactivated 这两个生命周期钩子函数将会被对应执行。

> 什么是 `options.abstract`？
> 走了 render 的组件是在哪里处理生命周期钩子的？它们还有原来的生命周期吗？
> 为什么只能处理第一个子元素？为什么要这么设计？
> render 返回 vnode 就能用来渲染？
> keep-alive 包 router-view 有什么用？
> 更多业务上的使用？
> 参考 keep-alive 能写出什么样的组件代码？
> LRU？

### 挂载

### 自定义渲染

### 缓存

### 卸载

### source code reference

### vue 3.0 的实现

### 参考

## vue event

## v-model

## slot

## transition

## 计算属性 vs 侦听属性

## 生命周期

## props

## 编译优化

## provide & inject