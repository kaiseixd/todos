/**
 * PropertyKey
 */
// string | number | symbol

/**
 * tuple
 */
// tuple[number]

/**
 * typeof
 */
const tuple = ['tesla', 'model 3', 'model X', 'model Y'] as const
type TypeofTupleFoo = typeof tuple

/**
 * keyof T
 */

/**
 * [key in keys expression]
 */

/**
 * extends 泛型约束
 * 1. extends object
 * 2. extends 'a' | 'b'
 */

// 1.
interface Lengthwise {
  length: number;
}

function loggingIdentity<T extends Lengthwise>(arg: T): T {
  console.log(arg.length);  // Now we know it has a .length property, so no more error
  return arg;
}

loggingIdentity([]);
loggingIdentity({ length: 1, value: 1 });

// 2.
interface Lengthwise2 {
  length: number;
}

function loggingIdentity2<T extends keyof Lengthwise2>(arg: T): T {
  return arg;
}

loggingIdentity2('length');
loggingIdentity2('a');

/** 
 * readonly
 */
type FooForReadonly = {
  bar: number;
  bas: number;
};

type ReadonlyFoo = {
  readonly bar: number;
  readonly bas: number;
};

type ReadonlyFoo2 = Readonly<FooForReadonly>;

/**
 * 怎么表示一个数组的子集
 */